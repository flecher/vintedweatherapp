package com.kanzi.vintedhomework.forecast;

import android.util.Log;

import com.kanzi.vintedhomework.AreaKeyStorage;
import com.kanzi.vintedhomework.models.ForecastModel;
import com.kanzi.vintedhomework.networking.NetworkingClient;

import io.reactivex.observers.DisposableObserver;

/**
 * Created by Benas on 3/7/2018.
 */

public class ForecastPresenterImpl implements ForecastPresenter {

    private static final String TAG = ForecastPresenterImpl.class.getSimpleName();

    private ForecastView forecastView;

    ForecastPresenterImpl(ForecastView forecastView) {
        this.forecastView = forecastView;
    }

    @Override
    public void onResume() {
        if (forecastView != null) {
            forecastView.showProgress();
        }
        getForecast(AreaKeyStorage.getAreaKey());
    }

    @Override
    public void getForecast(String areaKey) {
        NetworkingClient
                .getNetowrkingClient()
                .getForecast(areaKey)
                .subscribeWith(getObserver());

    }

    private DisposableObserver<ForecastModel> getObserver() {
        return new DisposableObserver<ForecastModel>() {
            @Override
            public void onNext(ForecastModel forecastModels) {
                forecastView.showForecastWeather(forecastModels.getDailyForecasts());
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "Failure fetching forecast", e);
            }

            @Override
            public void onComplete() {

            }
        };
    }
}
