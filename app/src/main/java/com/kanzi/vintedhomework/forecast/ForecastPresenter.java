package com.kanzi.vintedhomework.forecast;

/**
 * Created by Benas on 3/7/2018.
 */

public interface ForecastPresenter {
    void onResume();

    void getForecast(String areaKey);
}
