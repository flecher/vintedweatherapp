package com.kanzi.vintedhomework.forecast;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kanzi.vintedhomework.AreaKeyStorage;
import com.kanzi.vintedhomework.R;
import com.kanzi.vintedhomework.main.Updatable;
import com.kanzi.vintedhomework.adapters.ForecastAdapter;
import com.kanzi.vintedhomework.models.DailyForecasts;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Benas on 3/7/2018.
 */

public class ForecastFragment extends Fragment implements ForecastView, Updatable {

    View rootView;
    private Unbinder unbinder;
    private ForecastPresenter forecastPresenter;
    @BindView(R.id.forecast_list)
    RecyclerView forecastView;
    RecyclerView.Adapter adapter;
    ProgressDialog pd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.forecast, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        forecastView.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        pd = new ProgressDialog(this.getActivity());
        forecastPresenter = new ForecastPresenterImpl(this);

        return rootView;
    }

    @Override
    public void showProgress() {
        pd.setMessage(getResources().getString(R.string.loading_text));
        pd.show();
    }

    @Override
    public void hideProgress() {
        pd.hide();
    }

    @Override
    public void showForecastWeather(List<DailyForecasts> forecastModel) {
        hideProgress();
        adapter = new ForecastAdapter(getContext(), forecastModel);
        forecastView.setAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void onResume() {
        super.onResume();
        forecastPresenter.onResume();
    }

    @Override
    public void update() {
        forecastPresenter.getForecast(AreaKeyStorage.getAreaKey());
    }
}
