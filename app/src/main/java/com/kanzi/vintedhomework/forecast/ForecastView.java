package com.kanzi.vintedhomework.forecast;

import com.kanzi.vintedhomework.models.DailyForecasts;

import java.util.List;

/**
 * Created by Benas on 3/7/2018.
 */

public interface ForecastView {
    void showProgress();

    void hideProgress();

    void showForecastWeather(List<DailyForecasts> forecastModel);
}
