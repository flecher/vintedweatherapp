package com.kanzi.vintedhomework.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Benas on 3/7/2018.
 */

public class ParametersModel {
    @SerializedName("Value")
    private double value;
    @SerializedName("Unit")
    private String unit;
    @SerializedName("UnitType")
    private int unitType;

    public ParametersModel(double value, String unit, int unitType) {
        this.value = value;
        this.unit = unit;
        this.unitType = unitType;
    }

    public double getValue() {
        return value;
    }

    public String getUnit() {
        return unit;
    }

    public int getUnitType() {
        return unitType;
    }
}
