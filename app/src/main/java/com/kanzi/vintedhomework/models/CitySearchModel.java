package com.kanzi.vintedhomework.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Benas on 3/4/2018.
 */

public class CitySearchModel {
    @SerializedName("Key")
    @Expose
    private String areaKey;

    public CitySearchModel(String KEY) {
        this.areaKey = KEY;
    }

    public String getAreaKey() {
        return areaKey;
    }
}
