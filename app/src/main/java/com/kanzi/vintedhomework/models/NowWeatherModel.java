package com.kanzi.vintedhomework.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Benas on 3/4/2018.
 */

public class NowWeatherModel {
    @SerializedName("LocalObservationDateTime")
    private String localObservationDateTime;
    @SerializedName("EpochTime")
    private long epochTime;
    @SerializedName("WeatherText")
    private String weatherText;
    @SerializedName("WeatherIcon")
    private int weatherIcon;
    @SerializedName("IsDayTime")
    private boolean isDayTime;
    @SerializedName("Temperature")
    private Temperature temperature;
    @SerializedName("RealFeelTemperature")
    private RealFeelTemperature realFeelTemperature;
    @SerializedName("RelativeHumidity")
    private int relativeHumidity;
    @SerializedName("Wind")
    private Wind wind;
    @SerializedName("Visibility")
    private Visibility visibility;
    @SerializedName("TemperatureSummary")
    private TemperatureSummary temperatureSummary;


    public NowWeatherModel(String localObservationDateTime, long epochTime, String weatherText,
                           int weatherIcon, boolean isDayTime, Temperature temperature,
                           RealFeelTemperature realFeelTemperature, int relativeHumidity, Wind wind,
                           Visibility visibility, TemperatureSummary temperatureSummary) {
        this.localObservationDateTime = localObservationDateTime;
        this.epochTime = epochTime;
        this.weatherText = weatherText;
        this.weatherIcon = weatherIcon;
        this.isDayTime = isDayTime;
        this.temperature = temperature;
        this.realFeelTemperature = realFeelTemperature;
        this.relativeHumidity = relativeHumidity;
        this.wind = wind;
        this.visibility = visibility;
        this.temperatureSummary = temperatureSummary;
    }

    public TemperatureSummary getTemperatureSummary() {
        return temperatureSummary;
    }

    public int getWeatherIcon() {
        return weatherIcon;
    }

    public boolean isDayTime() {
        return isDayTime;
    }

    public Temperature getTemperature() {
        return temperature;
    }

    public RealFeelTemperature getRealFeelTemperature() {
        return realFeelTemperature;
    }

    public int getRelativeHumidity() {
        return relativeHumidity;
    }

    public Wind getWind() {
        return wind;
    }

    public Visibility getVisibility() {
        return visibility;
    }

    public String getLocalObservationDateTime() {
        return localObservationDateTime;
    }

    public long getEpochTime() {
        return epochTime;
    }

    public String getWeatherText() {
        return weatherText;
    }

    public class Temperature {
        @SerializedName("Metric")
        private ParametersModel metric;
        @SerializedName("Imperial")
        private ParametersModel imperial;

        public Temperature(ParametersModel metric, ParametersModel imperial) {
            this.metric = metric;
            this.imperial = imperial;
        }

        public ParametersModel getMetric() {
            return metric;
        }

        public ParametersModel getImperial() {
            return imperial;
        }
    }

    public class RealFeelTemperature {
        @SerializedName("Metric")
        private ParametersModel metric;
        @SerializedName("Imperial")
        private ParametersModel imperial;

        public RealFeelTemperature(ParametersModel metric, ParametersModel imperial) {
            this.metric = metric;
            this.imperial = imperial;
        }

        public ParametersModel getMetric() {
            return metric;
        }

        public ParametersModel getImperial() {
            return imperial;
        }
    }

    public class Wind {
        @SerializedName("Speed")
        private Speed speed;

        public Wind(Speed speed) {
            this.speed = speed;
        }

        public Speed getSpeed() {
            return speed;
        }

        public class Speed {
            @SerializedName("Metric")
            private ParametersModel metric;

            public Speed(ParametersModel metric) {
                this.metric = metric;
            }

            public ParametersModel getMetric() {
                return metric;
            }
        }
    }

    public class Visibility {
        @SerializedName("Metric")
        private ParametersModel metric;

        public Visibility(ParametersModel metric) {
            this.metric = metric;
        }

        public ParametersModel getMetric() {
            return metric;
        }
    }

    public class TemperatureSummary {
        @SerializedName("Past6HourRange")
        private Past6HourRange past6HourRange;

        public TemperatureSummary(Past6HourRange past6HourRange) {
            this.past6HourRange = past6HourRange;
        }

        public Past6HourRange getPast6HourRange() {
            return past6HourRange;
        }

        public class Past6HourRange {
            @SerializedName("Minimum")
            private RangeModel minimum;
            @SerializedName("Maximum")
            private RangeModel maximum;

            public Past6HourRange(RangeModel minimum, RangeModel maximum) {
                this.minimum = minimum;
                this.maximum = maximum;
            }

            public RangeModel getMinimum() {
                return minimum;
            }

            public RangeModel getMaximum() {
                return maximum;
            }

            public class RangeModel {
                @SerializedName("Metric")
                private ParametersModel metric;

                public RangeModel(ParametersModel metric) {
                    this.metric = metric;
                }

                public ParametersModel getMetric() {
                    return metric;
                }
            }
        }
    }
}
