package com.kanzi.vintedhomework.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Benas on 3/10/2018.
 */

public class AutocompleteSearchModel {
    @SerializedName("Version")
    private int version;
    @SerializedName("Key")
    private long cityKey;
    @SerializedName("Type")
    private String type;
    @SerializedName("Rank")
    private int rank;
    @SerializedName("LocalizedName")
    private String localizedName;

    public AutocompleteSearchModel(int version, long cityKey, String type, int rank, String localizedName) {
        this.version = version;
        this.cityKey = cityKey;
        this.type = type;
        this.rank = rank;
        this.localizedName = localizedName;
    }

    public int getVersion() {
        return version;
    }

    public long getCityKey() {
        return cityKey;
    }

    public String getType() {
        return type;
    }

    public int getRank() {
        return rank;
    }

    public String getLocalizedName() {
        return localizedName;
    }
}
