package com.kanzi.vintedhomework.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Benas on 3/7/2018.
 */

public class HeadlineModel {
    @SerializedName("EffectiveDate")
    private String effectiveDate;
    @SerializedName("EffectiveEpochDate")
    private long effectiveEpochDate;
    @SerializedName("Severity")
    private int severity;
    @SerializedName("Text")
    private String text;
    @SerializedName("Category")
    private String category;
    @SerializedName("EndDate")
    private String endDate;
    @SerializedName("EndEpochDate")
    private long endEpochDate;

    public HeadlineModel(String effectiveDate, long effectiveEpochDate, int severity, String text, String category, String endDate, long endEpochDate) {
        this.effectiveDate = effectiveDate;
        this.effectiveEpochDate = effectiveEpochDate;
        this.severity = severity;
        this.text = text;
        this.category = category;
        this.endDate = endDate;
        this.endEpochDate = endEpochDate;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public long getEffectiveEpochDate() {
        return effectiveEpochDate;
    }

    public int getSeverity() {
        return severity;
    }

    public String getText() {
        return text;
    }

    public String getCategory() {
        return category;
    }

    public String getEndDate() {
        return endDate;
    }

    public long getEndEpochDate() {
        return endEpochDate;
    }
}
