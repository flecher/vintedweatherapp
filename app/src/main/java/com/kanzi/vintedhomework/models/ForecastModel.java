package com.kanzi.vintedhomework.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Benas on 3/7/2018.
 */

public class ForecastModel {
    @SerializedName("Headline")
    private HeadlineModel headlineModel;
    @SerializedName("DailyForecasts")
    private List<DailyForecasts> dailyForecasts;

    public ForecastModel(HeadlineModel headlineModel, List<DailyForecasts> dailyForecasts) {
        this.headlineModel = headlineModel;
        this.dailyForecasts = dailyForecasts;
    }

    public HeadlineModel getHeadlineModel() {
        return headlineModel;
    }

    public List<DailyForecasts> getDailyForecasts() {
        return dailyForecasts;
    }
}
