package com.kanzi.vintedhomework.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Benas on 3/7/2018.
 */

public class DailyForecasts {
    @SerializedName("Date")
    private String date;
    @SerializedName("EpochDate")
    private long epochDate;
    @SerializedName("Temperature")
    private ForecastTemperature forecastTemperature;
    @SerializedName("Day")
    private DayTime dayTime;
    @SerializedName("Night")
    private DayTime nightTime;

    public DailyForecasts(String date, long epochDate, ForecastTemperature forecastTemperature,
                          DayTime dayTime, DayTime nightTime) {
        this.date = date;
        this.epochDate = epochDate;
        this.forecastTemperature = forecastTemperature;
        this.dayTime = dayTime;
        this.nightTime = nightTime;
    }

    public String getDate() {
        return date;
    }

    public long getEpochDate() {
        return epochDate;
    }

    public ForecastTemperature getForecastTemperature() {
        return forecastTemperature;
    }

    public DayTime getDayTime() {
        return dayTime;
    }

    public DayTime getNightTime() {
        return nightTime;
    }

    public class DayTime {
        @SerializedName("Icon")
        private int icon;
        @SerializedName("IconPhrase")
        private String iconPhrase;

        public DayTime(int icon, String iconPhrase) {
            this.icon = icon;
            this.iconPhrase = iconPhrase;
        }

        public int getIcon() {
            return icon;
        }

        public String getIconPhrase() {
            return iconPhrase;
        }
    }

    public class ForecastTemperature {
        @SerializedName("Minimum")
        private ParametersModel minimum;
        @SerializedName("Maximum")
        private ParametersModel maximum;

        public ForecastTemperature(ParametersModel minimum, ParametersModel maximum) {
            this.minimum = minimum;
            this.maximum = maximum;
        }

        public ParametersModel getMinimum() {
            return minimum;
        }

        public ParametersModel getMaximum() {
            return maximum;
        }
    }
}
