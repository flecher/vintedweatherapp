package com.kanzi.vintedhomework.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Benas on 3/5/2018.
 */

public class HourlyWeatherModel {
    @SerializedName("DateTime")
    private String dateTime;
    @SerializedName("EpochDateTime")
    private long epochDateTime;
    @SerializedName("WeatherIcon")
    private int weatherIcon;
    @SerializedName("IconPhrase")
    private String iconPhrase;
    @SerializedName("IsDaylight")
    private boolean isDayLight;
    @SerializedName("PrecipitationProbability")
    private int precipitationProbability;
    @SerializedName("Temperature")
    private ParametersModel temperature;

    public HourlyWeatherModel(String dateTime, long epochDateTime, int weatherIcon, String iconPhrase,
                              boolean isDayLight, int precipitationProbability,
                              ParametersModel temperature) {
        this.dateTime = dateTime;
        this.epochDateTime = epochDateTime;
        this.weatherIcon = weatherIcon;
        this.iconPhrase = iconPhrase;
        this.isDayLight = isDayLight;
        this.precipitationProbability = precipitationProbability;
        this.temperature = temperature;
    }

    public String getDateTime() {
        return dateTime;
    }

    public long getEpochDateTime() {
        return epochDateTime;
    }

    public int getWeatherIcon() {
        return weatherIcon;
    }

    public String getIconPhrase() {
        return iconPhrase;
    }

    public boolean isDayLight() {
        return isDayLight;
    }

    public int getPrecipitationProbability() {
        return precipitationProbability;
    }

    public ParametersModel getTemperature() {
        return temperature;
    }
}
