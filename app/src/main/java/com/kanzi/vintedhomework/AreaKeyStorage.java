package com.kanzi.vintedhomework;

/**
 * Created by Benas on 3/10/2018.
 */

public class AreaKeyStorage {
    private static String areaKey = "231459";

    private AreaKeyStorage() {
    }

    public static String getAreaKey() {
        return areaKey;
    }

    public static void setAreaKey(String areaKey) {
        AreaKeyStorage.areaKey = areaKey;
    }
}
