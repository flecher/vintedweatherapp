package com.kanzi.vintedhomework.utils;

import org.joda.time.DateTime;

/**
 * Created by Benas on 3/7/2018.
 */

public class Utilities {

    public static String constructImgUrl(int imgId) {
        StringBuilder urlBuilder = new StringBuilder();
        urlBuilder.append("https://apidev.accuweather.com/developers/Media/Default/WeatherIcons/");
        if (imgId < 10) {
            urlBuilder.append("0").append(imgId);
        } else {
            urlBuilder.append(imgId);
        }
        urlBuilder.append("-s.png");
        return urlBuilder.toString();
    }

    public static String convertDateTimeToHours(String date) {
        DateTime dateTime = DateTime.parse(date);
        return dateTime.hourOfDay().getAsText();
    }

    public static String convertDateTimeToWeekDay(String date) {
        DateTime dateTime = DateTime.parse(date);
        return dateTime.dayOfWeek().getAsText();
    }

}
