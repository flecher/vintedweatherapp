package com.kanzi.vintedhomework.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kanzi.vintedhomework.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Benas on 3/11/2018.
 */

public class HourlyWeatherHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.time_text)
    TextView time;
    @BindView(R.id.weather_text)
    TextView weather;
    @BindView(R.id.temperature_text)
    TextView temperature;
    @BindView(R.id.weather_icon)
    ImageView weatherIcon;

    public HourlyWeatherHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
