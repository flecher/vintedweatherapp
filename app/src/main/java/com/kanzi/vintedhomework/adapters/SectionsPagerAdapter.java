package com.kanzi.vintedhomework.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kanzi.vintedhomework.forecast.ForecastFragment;
import com.kanzi.vintedhomework.hourly.HourlyFragment;
import com.kanzi.vintedhomework.now.NowFragment;

/**
 * Created by Benas on 3/11/2018.
 */

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new NowFragment();
            case 1:
                return new HourlyFragment();
            case 2:
                return new ForecastFragment();
        }
        return null;
    }

    @Override
    public int getItemPosition(Object object) {
        if (object instanceof NowFragment) {
            ((NowFragment) object).update();
        }else if (object instanceof HourlyFragment) {
            ((HourlyFragment) object).update();
        }else if (object instanceof ForecastFragment) {
            ((ForecastFragment) object).update();
        }
        return super.getItemPosition(object);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
