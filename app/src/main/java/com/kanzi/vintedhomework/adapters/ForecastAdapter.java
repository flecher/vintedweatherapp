package com.kanzi.vintedhomework.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.bumptech.glide.Glide;
import com.kanzi.vintedhomework.R;
import com.kanzi.vintedhomework.models.DailyForecasts;
import com.kanzi.vintedhomework.utils.Utilities;

import java.util.List;

/**
 * Created by Benas on 3/7/2018.
 */

public class ForecastAdapter extends RecyclerView.Adapter<ForecastHolder> {

    private Context context;
    private List<DailyForecasts> forecast;

    public ForecastAdapter(Context context, List<DailyForecasts> forecast) {
        this.context = context;
        this.forecast = forecast;
    }

    @Override
    public ForecastHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_forecast, parent, false);
        return new ForecastHolder(view);
    }

    @Override
    public void onBindViewHolder(ForecastHolder holder, int position) {
        DailyForecasts dailyForecasts = forecast.get(position);
        String weatherPhrase = dailyForecasts.getDayTime().getIconPhrase();
        String date = Utilities.convertDateTimeToWeekDay(dailyForecasts.getDate());
        double maximumTemperature = dailyForecasts.getForecastTemperature().getMaximum().getValue();
        String temperatureUnit = dailyForecasts.getForecastTemperature().getMaximum().getUnit();
        double minimumTemperature = dailyForecasts.getForecastTemperature().getMinimum().getValue();

        holder.weatherText.setText(weatherPhrase);
        holder.weekDayText.setText(date);
        holder.temperatureMaxText.setText(maximumTemperature + temperatureUnit);
        holder.temperatureMinText.setText(minimumTemperature + temperatureUnit);
        Glide.with(context)
                .load(Utilities.constructImgUrl(dailyForecasts.getDayTime().getIcon()))
                .into(holder.forecastWeatherIcon);
    }

    @Override
    public int getItemCount() {
        return forecast.size();
    }
}
