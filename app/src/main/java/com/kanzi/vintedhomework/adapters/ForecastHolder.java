package com.kanzi.vintedhomework.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.kanzi.vintedhomework.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Benas on 3/11/2018.
 */

public class ForecastHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.forecast_weather_text)
    TextView weatherText;
    @BindView(R.id.week_day_text)
    TextView weekDayText;
    @BindView(R.id.temperature_max_text)
    TextView temperatureMaxText;
    @BindView(R.id.temperature_min_text)
    TextView temperatureMinText;
    @BindView(R.id.forecast_weather_icon)
    ImageView forecastWeatherIcon;

    ForecastHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }
}
