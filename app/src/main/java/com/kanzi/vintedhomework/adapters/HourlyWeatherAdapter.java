package com.kanzi.vintedhomework.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kanzi.vintedhomework.R;
import com.kanzi.vintedhomework.models.HourlyWeatherModel;
import com.kanzi.vintedhomework.utils.Utilities;

import org.joda.time.DateTime;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Benas on 3/5/2018.
 */

public class HourlyWeatherAdapter extends RecyclerView.Adapter<HourlyWeatherHolder> {

    private Context context;
    private List<HourlyWeatherModel> weatherList;

    public HourlyWeatherAdapter(Context context, List<HourlyWeatherModel> weatherList) {
        this.context = context;
        this.weatherList = weatherList;
    }

    @Override
    public HourlyWeatherHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.row_hourly_weather, parent, false);
        return new HourlyWeatherHolder(view);
    }

    @Override
    public void onBindViewHolder(HourlyWeatherHolder holder, int position) {
        String temperature = String.valueOf(weatherList.get(position).getTemperature().getValue());
        String time = Utilities.convertDateTimeToHours(weatherList.get(position).getDateTime());
        String weatherPhrase = weatherList.get(position).getIconPhrase();

        holder.temperature.setText(temperature);
        holder.time.setText(time);
        holder.weather.setText(weatherPhrase);
        Glide.with(context)
                .load(Utilities.constructImgUrl(weatherList.get(position).getWeatherIcon()))
                .into(holder.weatherIcon);
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }
}
