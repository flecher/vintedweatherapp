package com.kanzi.vintedhomework.hourly;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kanzi.vintedhomework.AreaKeyStorage;
import com.kanzi.vintedhomework.R;
import com.kanzi.vintedhomework.main.Updatable;
import com.kanzi.vintedhomework.adapters.HourlyWeatherAdapter;
import com.kanzi.vintedhomework.models.HourlyWeatherModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Benas on 3/5/2018.
 */

public class HourlyFragment extends Fragment implements HourlyView, Updatable {

    View rootView;
    private Unbinder unbinder;
    HourlyPresenter hourlyPresenter;
    @BindView(R.id.hourly_list)
    RecyclerView hourlyList;
    RecyclerView.Adapter adapter;
    ProgressDialog pd;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.hourly, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        hourlyList.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
        pd = new ProgressDialog(this.getActivity());
        hourlyPresenter = new HourlyPresenterImpl(this);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        hourlyPresenter.onResume();
    }

    @Override
    public void showProgress() {
        pd.setMessage(getResources().getString(R.string.loading_text));
        pd.show();
    }

    @Override
    public void hideProgress() {
        pd.hide();
    }

    @Override
    public void showHourlyWeather(List<HourlyWeatherModel> hourlyWeatherModels) {
        hideProgress();
        adapter = new HourlyWeatherAdapter(getContext(), hourlyWeatherModels);
        hourlyList.setAdapter(adapter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }

    @Override
    public void update() {
        hourlyPresenter.getHourlyWeather(AreaKeyStorage.getAreaKey());
    }
}
