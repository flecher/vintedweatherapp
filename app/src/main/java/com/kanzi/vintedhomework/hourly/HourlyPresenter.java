package com.kanzi.vintedhomework.hourly;

/**
 * Created by Benas on 3/5/2018.
 */

public interface HourlyPresenter {
    void onResume();

    void getHourlyWeather(String areaKey);
}
