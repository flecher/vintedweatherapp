package com.kanzi.vintedhomework.hourly;

import com.kanzi.vintedhomework.models.HourlyWeatherModel;

import java.util.List;

/**
 * Created by Benas on 3/5/2018.
 */

public interface HourlyView {
    void showProgress();

    void hideProgress();

    void showHourlyWeather(List<HourlyWeatherModel> hourlyWeatherModel);
}
