package com.kanzi.vintedhomework.hourly;

import android.util.Log;

import com.kanzi.vintedhomework.AreaKeyStorage;
import com.kanzi.vintedhomework.models.CitySearchModel;
import com.kanzi.vintedhomework.models.HourlyWeatherModel;
import com.kanzi.vintedhomework.networking.NetworkingClient;

import java.util.List;

import io.reactivex.observers.DisposableObserver;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Benas on 3/5/2018.
 */

public class HourlyPresenterImpl implements HourlyPresenter {

    private static final String TAG = HourlyPresenterImpl.class.getSimpleName();

    private HourlyView hourlyView;

    HourlyPresenterImpl(HourlyView hourlyView) {
        this.hourlyView = hourlyView;
    }

    @Override
    public void onResume() {
        if (hourlyView != null) {
            hourlyView.showProgress();
        }
        getHourlyWeather(AreaKeyStorage.getAreaKey());
    }

    @Override
    public void getHourlyWeather(String areaKey) {
        NetworkingClient
                .getNetowrkingClient()
                .getHourlyWeather(areaKey)
                .subscribeWith(getObserver());
    }

    private DisposableObserver<List<HourlyWeatherModel>> getObserver() {
        return new DisposableObserver<List<HourlyWeatherModel>>() {
            @Override
            public void onNext(List<HourlyWeatherModel> hourlyWeatherModels) {
                hourlyView.showHourlyWeather(hourlyWeatherModels);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "Failure fetching hourly weather", e);
            }

            @Override
            public void onComplete() {

            }
        };
    }
}
