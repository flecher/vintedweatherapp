package com.kanzi.vintedhomework.now;

import com.kanzi.vintedhomework.models.NowWeatherModel;

/**
 * Created by Benas on 3/4/2018.
 */

public interface NowView {
    void showProgress();

    void hideProgress();

    void showCityWeather(NowWeatherModel items);

}
