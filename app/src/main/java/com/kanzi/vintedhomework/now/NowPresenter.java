package com.kanzi.vintedhomework.now;

/**
 * Created by Benas on 3/4/2018.
 */

public interface NowPresenter {
    void onResume();

    void getWeather(String areaKey);
}
