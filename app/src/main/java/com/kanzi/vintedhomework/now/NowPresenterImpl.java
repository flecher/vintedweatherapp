package com.kanzi.vintedhomework.now;

import android.util.Log;

import com.kanzi.vintedhomework.AreaKeyStorage;
import com.kanzi.vintedhomework.models.NowWeatherModel;
import com.kanzi.vintedhomework.networking.NetworkingClient;

import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;

/**
 * Created by Benas on 3/4/2018.
 */

public class NowPresenterImpl implements NowPresenter {

    private static final String TAG = NowPresenterImpl.class.getSimpleName();

    private NowView nowView;

    NowPresenterImpl(NowView nowView) {
        this.nowView = nowView;
    }

    @Override
    public void onResume() {
        if (nowView != null) {
            nowView.showProgress();
        }
        getWeather(AreaKeyStorage.getAreaKey());
    }

    @Override
    public void getWeather(String areaKey) {
        NetworkingClient
                .getNetowrkingClient()
                .getNowWeather(areaKey)
                .subscribeWith(getObserver());
    }


    private DisposableObserver<List<NowWeatherModel>> getObserver() {
        return new DisposableObserver<List<NowWeatherModel>>() {
            @Override
            public void onNext(@NonNull List<NowWeatherModel> nowWeatherModel) {
                nowView.showCityWeather(nowWeatherModel.get(0));
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "Failure fetching now weather", e);
            }

            @Override
            public void onComplete() {

            }
        };
    }
}
