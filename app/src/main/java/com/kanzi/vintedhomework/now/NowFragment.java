package com.kanzi.vintedhomework.now;

import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.kanzi.vintedhomework.AreaKeyStorage;
import com.kanzi.vintedhomework.R;
import com.kanzi.vintedhomework.main.Updatable;
import com.kanzi.vintedhomework.models.NowWeatherModel;
import com.kanzi.vintedhomework.utils.Utilities;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by Benas on 3/1/2018.
 */

public class NowFragment extends Fragment implements NowView, Updatable {

    View rootView;
    private Unbinder unbinder;
    @BindView(R.id.weather_text)
    TextView weatherText;
    @BindView(R.id.now_temperature_text)
    TextView nowTemperatureText;
    @BindView(R.id.high_low_temperatures)
    TextView highLowTemperatureText;
    @BindView(R.id.real_feel_temperature)
    TextView realFeelTemperatureText;
    @BindView(R.id.humidity_text)
    TextView humidityText;
    @BindView(R.id.winds_text)
    TextView windsText;
    @BindView(R.id.visibility_text)
    TextView visibilityText;
    @BindView(R.id.small_image)
    ImageView smallImage;
    @BindView(R.id.big_image)
    ImageView bigImage;
    ProgressDialog pd;


    private NowPresenter presenter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.now, container, false);
        unbinder = ButterKnife.bind(this, rootView);
        presenter = new NowPresenterImpl(this);
        pd = new ProgressDialog(this.getActivity());
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void showProgress() {
        pd.setMessage(getResources().getString(R.string.loading_text));
        pd.show();
    }

    @Override
    public void hideProgress() {
        pd.hide();
    }

    @Override
    public void showCityWeather(NowWeatherModel items) {
        hideProgress();

        double temperature = items.getTemperature().getMetric().getValue();
        String temperatureUnit = items.getTemperature().getMetric().getUnit();
        String weatherPhrase = items.getWeatherText();
        double realFeelTemperature = items.getRealFeelTemperature().getMetric().getValue();
        String humidity = String.valueOf(items.getRelativeHumidity());
        double windSpeed = items.getWind().getSpeed().getMetric().getValue();
        String windUnit = items.getWind().getSpeed().getMetric().getUnit();
        double visibility = items.getVisibility().getMetric().getValue();
        String visibilityUnit = items.getVisibility().getMetric().getUnit();
        double maximumTemperature = items
                .getTemperatureSummary()
                .getPast6HourRange()
                .getMaximum()
                .getMetric()
                .getValue();
        double minimumTemperature = items
                .getTemperatureSummary()
                .getPast6HourRange()
                .getMinimum()
                .getMetric()
                .getValue();

        nowTemperatureText.setText(temperature + " " + temperatureUnit);
        weatherText.setText(weatherPhrase);
        realFeelTemperatureText.setText(realFeelTemperature + " " + temperatureUnit);
        humidityText.setText(humidity);
        windsText.setText(windSpeed + " " + windUnit);
        visibilityText.setText(visibility + " " + visibilityUnit);
        highLowTemperatureText.setText("H " + maximumTemperature + " / L " + minimumTemperature);
        Glide.with(getContext())
                .load(Utilities.constructImgUrl(items.getWeatherIcon()))
                .into(smallImage);
        Glide.with(getContext())
                .load(Utilities.constructImgUrl(items.getWeatherIcon()))
                .into(bigImage);
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void update() {
        presenter.getWeather(AreaKeyStorage.getAreaKey());
    }
}
