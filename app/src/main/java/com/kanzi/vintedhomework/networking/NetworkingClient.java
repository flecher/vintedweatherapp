package com.kanzi.vintedhomework.networking;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.kanzi.vintedhomework.models.AutocompleteSearchModel;
import com.kanzi.vintedhomework.models.CitySearchModel;
import com.kanzi.vintedhomework.models.ForecastModel;
import com.kanzi.vintedhomework.models.HourlyWeatherModel;
import com.kanzi.vintedhomework.models.NowWeatherModel;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Benas on 3/4/2018.
 */

public class NetworkingClient {
    private static final String TAG = NetworkingClient.class.getSimpleName();
    private static final String BASE_URL = "http://dataservice.accuweather.com/";
    private static final String API_KEY = "Dr236At4tfMazVBh9gULghcFPUjYmTAa";
    private static Retrofit retrofit;
    private static NetworkingInterface networkingInterface;
    private static final NetworkingClient netowrkingClient = new NetworkingClient();

    private NetworkingClient() {
        if (networkingInterface == null) {
            networkingInterface = getClient().create(NetworkingInterface.class);
        }
    }

    public static NetworkingClient getNetowrkingClient() {
        return netowrkingClient;
    }

    private static Retrofit getClient() {
        if (retrofit == null) {
            try {
                OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
                Gson gson = new GsonBuilder().setLenient().create();

                retrofit = new Retrofit.Builder()
                        .baseUrl(BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .client(httpClient.build())
                        .build();
            } catch (Exception e) {
                Log.e(TAG, "FAILURE INITIALIZING RETROFIT", e);
            }
        }
        return retrofit;
    }

    public Call<String> getAllRegions() {
        return networkingInterface.getRegions(API_KEY);
    }

    public Observable<List<NowWeatherModel>> getNowWeather(String cityId) {
        return NetworkingClient.getClient().create(NetworkingInterface.class)
                .getNowWeather(cityId, API_KEY, true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<HourlyWeatherModel>> getHourlyWeather(String cityId) {
        return NetworkingClient.getClient().create(NetworkingInterface.class)
                .getHourlyWeather(cityId, API_KEY, true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Call<List<CitySearchModel>> getCityId(String cityName) {
        return networkingInterface.getCityId(API_KEY, cityName);
    }

    public Observable<ForecastModel> getForecast(String cityId) {
        return NetworkingClient.getClient().create(NetworkingInterface.class)
                .getForecast(cityId, API_KEY, true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public Observable<List<AutocompleteSearchModel>> cityAutocomplete(String text) {
        return NetworkingClient.getClient().create(NetworkingInterface.class)
                .cityAutocomplete(API_KEY, text)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}
