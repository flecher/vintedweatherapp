package com.kanzi.vintedhomework.networking;

import com.kanzi.vintedhomework.models.AutocompleteSearchModel;
import com.kanzi.vintedhomework.models.CitySearchModel;
import com.kanzi.vintedhomework.models.ForecastModel;
import com.kanzi.vintedhomework.models.HourlyWeatherModel;
import com.kanzi.vintedhomework.models.NowWeatherModel;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Benas on 3/4/2018.
 */

public interface NetworkingInterface {

    @GET("locations/v1/regions")
    Call<String> getRegions(@Query("apikey") String apiKey);

    @GET("locations/v1/cities/search")
    Call<List<CitySearchModel>> getCityId(@Query("apikey") String apiKey, @Query("q") String cityName);

    @GET("currentconditions/v1/{city_id}")
    Observable<List<NowWeatherModel>> getNowWeather(@Path("city_id") String cityId,
                                                    @Query("apikey") String apiKey,
                                                    @Query("details") boolean details);

    @GET("forecasts/v1/hourly/12hour/{city_id}")
    Observable<List<HourlyWeatherModel>> getHourlyWeather(@Path("city_id") String cityId,
                                                          @Query("apikey") String apiKey,
                                                          @Query("metric") Boolean metric);

    @GET("/forecasts/v1/daily/5day/{city_id}")
    Observable<ForecastModel> getForecast(@Path("city_id") String cityId,
                                          @Query("apikey") String apiKey,
                                          @Query("metric") Boolean metric);

    @GET("locations/v1/cities/autocomplete")
    Observable<List<AutocompleteSearchModel>> cityAutocomplete(@Query("apikey") String apiKey,
                                                               @Query("q") String text);
}