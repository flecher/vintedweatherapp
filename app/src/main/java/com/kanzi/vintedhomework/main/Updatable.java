package com.kanzi.vintedhomework.main;

/**
 * Created by Benas on 3/10/2018.
 */

public interface Updatable {
    void update();
}
