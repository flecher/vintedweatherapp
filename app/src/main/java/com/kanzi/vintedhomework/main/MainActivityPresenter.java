package com.kanzi.vintedhomework.main;

import com.jakewharton.rxbinding.widget.SearchViewQueryTextEvent;

import rx.Observer;

/**
 * Created by Benas on 3/10/2018.
 */

public interface MainActivityPresenter {
    void onResume();

    Observer<SearchViewQueryTextEvent> getSearchViewText();

    void getCityId(String city);
}
