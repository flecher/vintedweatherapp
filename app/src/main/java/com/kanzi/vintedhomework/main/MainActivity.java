package com.kanzi.vintedhomework.main;

import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CursorAdapter;
import android.widget.SearchView;

import com.kanzi.vintedhomework.R;
import com.kanzi.vintedhomework.adapters.SectionsPagerAdapter;

public class MainActivity extends AppCompatActivity implements MainActivityView, SearchView.OnQueryTextListener {

    private static final String TAG = MainActivity.class.getSimpleName();

    private SectionsPagerAdapter sectionsPagerAdapter;
    private SearchView searchView;
    private ViewPager viewPager;
    private MainActivityPresenter mainActivityPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        viewPager = findViewById(R.id.container);
        viewPager.setAdapter(sectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        searchView = (SearchView) MenuItemCompat.getActionView(item);
        mainActivityPresenter = new MainActivityPresenterImpl(this, this);
        mainActivityPresenter.onResume();
        if (searchView != null) {
//            RxSearchView.queryTextChangeEvents(searchView)
//                    .debounce(500, TimeUnit.MILLISECONDS)
//                    .distinctUntilChanged()
//                    .filter(charSequence -> !TextUtils.isEmpty(charSequence.queryText()))
//                    .subscribeOn(AndroidSchedulers.mainThread())
//                    .observeOn(Schedulers.io())
//                    .subscribe(mainActivityPresenter.getSearchViewText());
            searchView.setOnQueryTextListener(this);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        return id == R.id.action_settings || super.onOptionsItemSelected(item);
    }

    @Override
    public void populateSearchSuggestions(CursorAdapter adapter) {
        if (searchView == null) {
            Log.e(TAG, "Search view is null");
            return;
        }
        searchView.setSuggestionsAdapter(adapter);
    }

    @Override
    public void notifyAdapter() {
        sectionsPagerAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        mainActivityPresenter.getCityId(s);
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }


}
