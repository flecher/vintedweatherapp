package com.kanzi.vintedhomework.main;

import android.widget.CursorAdapter;

/**
 * Created by Benas on 3/10/2018.
 */

public interface MainActivityView {
    void populateSearchSuggestions(CursorAdapter adapter);

    void notifyAdapter();
}
