package com.kanzi.vintedhomework.main;

import android.content.Context;
import android.database.MatrixCursor;
import android.util.Log;
import android.widget.CursorAdapter;
import android.widget.SimpleCursorAdapter;

import com.jakewharton.rxbinding.widget.SearchViewQueryTextEvent;
import com.kanzi.vintedhomework.AreaKeyStorage;
import com.kanzi.vintedhomework.R;
import com.kanzi.vintedhomework.models.AutocompleteSearchModel;
import com.kanzi.vintedhomework.models.CitySearchModel;
import com.kanzi.vintedhomework.networking.NetworkingClient;

import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DisposableObserver;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observer;

/**
 * Created by Benas on 3/10/2018.
 */

public class MainActivityPresenterImpl implements MainActivityPresenter {

    private static final String TAG = MainActivityPresenterImpl.class.getSimpleName();

    private MainActivityView mainActivityView;
    private Context context;

    MainActivityPresenterImpl(MainActivityView mainActivityView,
                              Context context) {
        this.mainActivityView = mainActivityView;
        this.context = context;
    }

    @Override
    public void onResume() {
    }

    @Override
    public Observer<SearchViewQueryTextEvent> getSearchViewText() {
        return new Observer<SearchViewQueryTextEvent>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(SearchViewQueryTextEvent searchViewQueryTextEvent) {
                if (!searchViewQueryTextEvent.queryText().equals("")) {
                    NetworkingClient
                            .getNetowrkingClient()
                            .cityAutocomplete(searchViewQueryTextEvent.queryText().toString())
                            .subscribeWith(getSuggestions());
                }
            }
        };
    }

    private DisposableObserver<List<AutocompleteSearchModel>> getSuggestions() {
        return new DisposableObserver<List<AutocompleteSearchModel>>() {
            @Override
            public void onNext(@NonNull List<AutocompleteSearchModel> items) {
                mainActivityView.populateSearchSuggestions(createCursorAdapter(items));

            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG, "Failure fetching suggestions", e);
            }

            @Override
            public void onComplete() {

            }
        };
    }

    private CursorAdapter createCursorAdapter(List<AutocompleteSearchModel> items) {
        String[] columnNames = {"_id", "text"};
        MatrixCursor cursor = new MatrixCursor(columnNames);
        String[] temp = new String[2];
        int id = 0;
        for (AutocompleteSearchModel item : items) {
            temp[0] = String.valueOf(id++);
            temp[1] = item.getLocalizedName();
            cursor.addRow(temp);
        }
        String[] from = {"text"};
        int[] to = {R.id.city_name_text};
        return new SimpleCursorAdapter(context, R.layout.search_view_item, cursor, from, to);

    }

    @Override
    public void getCityId(String cityName) {
        Call<List<CitySearchModel>> call = NetworkingClient.getNetowrkingClient().getCityId(cityName);
        try {
            call.enqueue(new Callback<List<CitySearchModel>>() {
                @Override
                public void onResponse(@NonNull Call<List<CitySearchModel>> call,
                                       @NonNull Response<List<CitySearchModel>> response) {
                    List<CitySearchModel> list = response.body();
                    if(list == null){
                        Log.e(TAG, "Failure parsing CitySearchModel");
                        return;
                    }
                    if (!list.isEmpty()) {
                        AreaKeyStorage.setAreaKey(response.body().get(0).getAreaKey());
                        mainActivityView.notifyAdapter();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<List<CitySearchModel>> call,
                                      @NonNull Throwable t) {
                    Log.e(TAG, "Failure fetching citySearchModel", t);
                }
            });
        } catch (Exception e) {
            Log.e(TAG, "Failure fetching city id", e);
        }
    }


}
